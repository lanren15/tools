package com.zhuyan.hibernate.entity;

/**
 * 部门表，部门下面有多个雇员
 */
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "department")
public class Department {

	@Id
	@GenericGenerator(name = "myid", strategy = "increment")
	@GeneratedValue(generator = "myid")
	@Column(name = "id")
	private int id;
	@Column(name = "dept_name")
	private String deptName;
	@Column(name = "dept_tel")
	private String deptTel;

	/**
	 * 注意，这里都要放到字段上，或者全部都放到方法上才可以，否则，找不到
	 */
	@OneToMany(mappedBy = "belongDept", fetch = FetchType.LAZY)
	private List<Employ> emps = new ArrayList<Employ>();

	public Department() {
	}

	public Department(String deptName, String deptTel) {
		this.deptName = deptName;
		this.deptTel = deptTel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptTel() {
		return deptTel;
	}

	public void setDeptTel(String deptTel) {
		this.deptTel = deptTel;
	}

	public List<Employ> getEmps() {
		return emps;
	}

	public void setEmps(List<Employ> emps) {
		this.emps = emps;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Department [id=");
		builder.append(id);
		builder.append(", deptName=");
		builder.append(deptName);
		builder.append(", deptTel=");
		builder.append(deptTel);
		builder.append("]");
		return builder.toString();
	}

}
