package com.zhuyan.hibernate.entity;

/**
 * 雇员表
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "employ")
public class Employ {
	@Id
	@GenericGenerator(name = "myid", strategy = "increment")
	@GeneratedValue(generator = "myid")
	@Column(name = "id")
	private int id;

	@Column(name = "emp_name")
	private String empName;
	@Column(name = "emp_salary")
	private double empSalary;

	@Column(name = "emp_sex")
	private String empSex;

	/**
	 * 关联到部门表的主键，但是数据库上并没有关联，只是程序上做了关联<br>
	 * 注意，这里和下面的belongDept中使用了同一个字段，所以必须要设置insertable = false, updatable =
	 * false，否则报错
	 */
	@Column(name = "dept_id", insertable = false, updatable = false)
	private int deptId;

	/** 这里是做一个主表对象级联，需要主表的时候可以取这个，否则就只取上面的deptId */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "dept_id")
	private Department belongDept;

	public Employ() {
	}

	public Employ(String empName, double empSalary, String empSex) {
		this.empName = empName;
		this.empSalary = empSalary;
		this.empSex = empSex;
	}

	public Employ(String empName, double empSalary, String empSex, Department dept) {
		this.empName = empName;
		this.empSalary = empSalary;
		this.empSex = empSex;
		this.belongDept = dept;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public double getEmpSalary() {
		return empSalary;
	}

	public void setEmpSalary(double empSalary) {
		this.empSalary = empSalary;
	}

	public String getEmpSex() {
		return empSex;
	}

	public void setEmpSex(String empSex) {
		this.empSex = empSex;
	}

	public Department getBelongDept() {
		return belongDept;
	}

	public void setBelongDept(Department belongDept) {
		this.belongDept = belongDept;
	}

	public int getDeptId() {
		return deptId;
	}

	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employ [id=");
		builder.append(id);
		builder.append(", empName=");
		builder.append(empName);
		builder.append(", empSalary=");
		builder.append(empSalary);
		builder.append(", empSex=");
		builder.append(empSex);
		builder.append(", deptId=");
		builder.append(deptId);
		builder.append("]");
		return builder.toString();
	}

}
