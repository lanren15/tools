package com.zhuyan.hibernate.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

public class HibernateUtils {

	static SessionFactory sf = null;

	/**
	 * 注册服务
	 */
	static {
		final StandardServiceRegistry register = new StandardServiceRegistryBuilder().configure().build();
		sf = new MetadataSources(register).buildMetadata().buildSessionFactory();
	}

	/**
	 * 获取session
	 * 
	 * @return
	 */
	public static Session getSession() {
		return sf.openSession();
	}

	/**
	 * 关闭session
	 */
	public static void close(Session session) {
		if (session != null) {
			session.close();
		}
	}
}
