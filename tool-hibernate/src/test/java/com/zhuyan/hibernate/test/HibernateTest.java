package com.zhuyan.hibernate.test;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.zhuyan.hibernate.entity.Department;
import com.zhuyan.hibernate.entity.Employ;
import com.zhuyan.hibernate.utils.HibernateUtils;

public class HibernateTest {

	Session session = null;

	@Before
	public void setUp() throws Exception {
		session = HibernateUtils.getSession();
	}

	@After
	public void tearDown() throws Exception {
		HibernateUtils.close(session);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetDept() {
		List<Department> list = session.createQuery("from Department where id = 1 ").list();
		System.out.println(list.size());
		for (Department dept : list) {
			System.out.println("当前部门表对象：" + dept);
			System.out.println("================等待一下，测试懒加载");
			System.out.println("部门所属雇员表列表大小：" + dept.getEmps().size());
		}
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testGetEmp() {
		List<Employ> list = session.createQuery("from Employ where id = 1 ").list();
		System.out.println(list.size());
		for (Employ empoly : list) {
			System.out.println("当前雇员对象：" + empoly);
			System.out.println("================等待一下，测试懒加载");
			System.out.println("所属部门对象：" + empoly.getBelongDept());
		}
	}

	// @Test
	public void testAdd() {
		try {
			Transaction transaction = session.beginTransaction();
			Department dept = (Department) session.get(Department.class, 1);
			Employ employ = new Employ("lisi", 8287, "女");
			employ.setBelongDept(dept);

			session.save(employ);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {

		}
	}

	// @Test
	public void testUpdate() {
		try {
			Transaction transaction = session.beginTransaction();
			Department dept = (Department) session.get(Department.class, 1);
			Employ employ = new Employ("lisi", 8287, "女");
			employ.setBelongDept(dept);

			dept.setDeptName("测试部门名称");
			// 下面的update可以不用，因为commit的时候，会自动判断数据库中的值和内存中的值是否一致，不一致就会更新到数据库中的
			// session.update(dept);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
			session.getTransaction().rollback();
		} finally {

		}
	}
}
